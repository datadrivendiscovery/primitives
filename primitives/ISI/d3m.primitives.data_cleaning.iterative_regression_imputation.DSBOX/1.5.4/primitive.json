{
    "id": "f70b2324-1102-35f7-aaf6-7cd8e860acc4",
    "version": "1.5.4",
    "name": "DSBox Iterative Regression Imputer",
    "description": "Impute the missing value of numerical columns by iteratively regression using other numerical columns. It will fit\nand fill the missing value in the training set, and store the learned models. In the `produce` phase, it will use\nthe learned models to iteratively regress on the testing data again, and return the imputed testing data.",
    "python_path": "d3m.primitives.data_cleaning.iterative_regression_imputation.DSBOX",
    "primitive_family": "DATA_CLEANING",
    "algorithm_types": [
        "IMPUTATION"
    ],
    "source": {
        "name": "ISI",
        "contact": "mailto:kyao@isi.edu",
        "uris": [
            "https://gitlab.com/datadrivendiscovery/contrib/dsbox-primitives.git"
        ]
    },
    "keywords": [
        "preprocessing",
        "imputation"
    ],
    "installation": [
        {
            "type": "PIP",
            "package_uri": "git+https://gitlab.com/datadrivendiscovery/contrib/dsbox-primitives.git@2f4cb9adfe548d91ad973dc201f064cbda6f33e0#egg=dsbox-primitives"
        }
    ],
    "location_uris": [],
    "precondition": [
        "NO_CATEGORICAL_VALUES"
    ],
    "hyperparms_to_tune": [],
    "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
    "original_python_path": "dsbox.datapreprocessing.cleaner.iterative_regression.IterativeRegressionImputation",
    "primitive_code": {
        "class_type_arguments": {
            "Inputs": "d3m.container.pandas.DataFrame",
            "Outputs": "d3m.container.pandas.DataFrame",
            "Params": "dsbox.datapreprocessing.cleaner.iterative_regression.IR_Params",
            "Hyperparams": "dsbox.datapreprocessing.cleaner.iterative_regression.IterativeRegressionHyperparameter"
        },
        "interfaces_version": "2021.11.25.dev0",
        "interfaces": [
            "unsupervised_learning.UnsupervisedLearnerPrimitiveBase",
            "base.PrimitiveBase"
        ],
        "hyperparams": {
            "verbose": {
                "type": "d3m.metadata.hyperparams.UniformBool",
                "default": false,
                "structural_type": "bool",
                "semantic_types": [
                    "http://schema.org/Boolean",
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ]
            },
            "use_columns": {
                "type": "d3m.metadata.hyperparams.Set",
                "default": [],
                "structural_type": "typing.Sequence[int]",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "A set of column indices to force primitive to operate on. If any specified column cannot be parsed, it is skipped.",
                "elements": {
                    "type": "d3m.metadata.hyperparams.Hyperparameter",
                    "default": -1,
                    "structural_type": "int",
                    "semantic_types": []
                },
                "is_configuration": false,
                "min_size": 0
            },
            "exclude_columns": {
                "type": "d3m.metadata.hyperparams.Set",
                "default": [],
                "structural_type": "typing.Sequence[int]",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "A set of column indices to not operate on. Applicable only if \"use_columns\" is not provided.",
                "elements": {
                    "type": "d3m.metadata.hyperparams.Hyperparameter",
                    "default": -1,
                    "structural_type": "int",
                    "semantic_types": []
                },
                "is_configuration": false,
                "min_size": 0
            },
            "return_result": {
                "type": "d3m.metadata.hyperparams.Enumeration",
                "default": "replace",
                "structural_type": "str",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Should parsed columns be appended, should they replace original columns, or should only parsed columns be returned? This hyperparam is ignored if use_semantic_types is set to false.",
                "values": [
                    "append",
                    "replace",
                    "new"
                ]
            },
            "use_semantic_types": {
                "type": "d3m.metadata.hyperparams.UniformBool",
                "default": false,
                "structural_type": "bool",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Controls whether semantic_types metadata will be used for filtering columns in input dataframe. Setting this to false makes the code ignore return_result and will produce only the output dataframe"
            },
            "add_index_columns": {
                "type": "d3m.metadata.hyperparams.UniformBool",
                "default": true,
                "structural_type": "bool",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Also include primary index columns if input data has them. Applicable only if \"return_result\" is set to \"new\"."
            }
        },
        "arguments": {
            "hyperparams": {
                "type": "dsbox.datapreprocessing.cleaner.iterative_regression.IterativeRegressionHyperparameter",
                "kind": "RUNTIME"
            },
            "timeout": {
                "type": "typing.Union[NoneType, float]",
                "kind": "RUNTIME",
                "default": null
            },
            "iterations": {
                "type": "typing.Union[NoneType, int]",
                "kind": "RUNTIME",
                "default": null
            },
            "produce_methods": {
                "type": "typing.Sequence[str]",
                "kind": "RUNTIME"
            },
            "inputs": {
                "type": "d3m.container.pandas.DataFrame",
                "kind": "PIPELINE"
            },
            "params": {
                "type": "dsbox.datapreprocessing.cleaner.iterative_regression.IR_Params",
                "kind": "RUNTIME"
            }
        },
        "class_methods": {},
        "instance_methods": {
            "__init__": {
                "kind": "OTHER",
                "arguments": [
                    "hyperparams"
                ],
                "returns": "NoneType"
            },
            "fit": {
                "kind": "OTHER",
                "arguments": [
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]",
                "description": "train imputation parameters. Now support:\n-> greedySearch\n\nfor the method that not trainable, do nothing:\n-> interatively regression\n-> other\n\nParameters:\n----------\ndata: pandas dataframe\nlabel: pandas series, used for the trainable methods\n\nParameters\n----------\ntimeout:\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA ``CallResult`` with ``None`` value."
            },
            "fit_multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to ``set_training_data`` and all produce methods.\ntimeout:\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "get_params": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "dsbox.datapreprocessing.cleaner.iterative_regression.IR_Params",
                "description": "Returns parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nParameters should include all data which is necessary to create a new instance of this primitive\nbehaving exactly the same as this instance, when the new instance is created by passing the same\nparameters to the class constructor and calling ``set_params``.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nReturns\n-------\nAn instance of parameters."
            },
            "multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults. Despite accepting all arguments they can be passed as ``None`` by the caller\nwhen they are not needed by any of the produce methods in ``produce_methods``.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to all produce methods.\ntimeout:\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "produce": {
                "kind": "PRODUCE",
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "precond: run fit() before\n\nto complete the data, based on the learned parameters, support:\n-> greedy search\n\nalso support the untrainable methods:\n-> iteratively regression\n-> other\n\nParameters:\n----------\ndata: pandas dataframe\nlabel: pandas series, used for the evaluation of imputation\n\nTODO:\n----------\n1. add evaluation part for __simpleImpute()\n\nParameters\n----------\ninputs:\n    The inputs of shape [num_inputs, ...].\ntimeout:\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nThe outputs of shape [num_inputs, ...] wrapped inside ``CallResult``."
            },
            "set_params": {
                "kind": "OTHER",
                "arguments": [
                    "params"
                ],
                "returns": "NoneType",
                "description": "Sets parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nParameters\n----------\nparams:\n    An instance of parameters."
            },
            "set_training_data": {
                "kind": "OTHER",
                "arguments": [
                    "inputs"
                ],
                "returns": "NoneType",
                "description": "Sets training data of this primitive.\n\nParameters\n----------\ninputs : Input\n    The inputs."
            }
        },
        "class_attributes": {
            "logger": "logging.Logger",
            "metadata": "d3m.metadata.base.PrimitiveMetadata"
        },
        "instance_attributes": {
            "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
            "random_seed": "int",
            "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
            "volumes": "typing.Dict[str, str]",
            "temporary_directory": "typing.Union[NoneType, str]"
        },
        "params": {
            "fitted": "typing.Union[NoneType, typing.Any]",
            "verbose": "typing.Union[NoneType, typing.Any]",
            "iterations_done": "typing.Union[NoneType, typing.Any]",
            "has_finished": "typing.Union[NoneType, typing.Any]",
            "best_imputation": "typing.Union[NoneType, typing.Any]",
            "numeric_column_indices": "typing.Union[NoneType, typing.Any]"
        }
    },
    "structural_type": "dsbox.datapreprocessing.cleaner.iterative_regression.IterativeRegressionImputation",
    "digest": "3f786c0561d3964642498b666bde6416a08818c64b18f6485cc66c6f9cdae764"
}

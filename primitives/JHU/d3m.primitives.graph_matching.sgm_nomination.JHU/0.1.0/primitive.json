{
    "algorithm_types": [
        "RANDOM_GRAPH"
    ],
    "description": "Creates a similarity matrix from pairwise distances and nominates one-to-one\nsmallest distance vertex match.",
    "digest": "28cf19c300b16949094c84354c85fb0d5d2e62950bd17502c23e98e251e5f8d2",
    "hyperparams_configuration": {},
    "id": "8a913585-c269-4b25-ad32-40feda3c540e",
    "installation": [
        {
            "package": "libxml2-dev",
            "type": "UBUNTU",
            "version": "2.9.4"
        },
        {
            "package": "libpcre3-dev",
            "type": "UBUNTU",
            "version": "2.9.4"
        },
        {
            "package_uri": "git+https://gitlab.com/datadrivendiscovery/contrib/jhu-primitives.git@97dc5cabdbccf039ca46a7760c754f087274e662#egg=jhu_primitives",
            "type": "PIP"
        }
    ],
    "keywords": [
        "nomination",
        "matching",
        "linear sum assignment"
    ],
    "name": "jhu.sgm_nomination",
    "original_python_path": "jhu_primitives.sgm_nomination.sgm_nomination.SgmNomination",
    "preconditions": [
        "NO_MISSING_VALUES"
    ],
    "primitive_code": {
        "arguments": {
            "docker_containers": {
                "default": null,
                "kind": "RUNTIME",
                "type": "typing.Union[NoneType, typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]]"
            },
            "hyperparams": {
                "kind": "RUNTIME",
                "type": "jhu_primitives.sgm_nomination.sgm_nomination.Hyperparams"
            },
            "inputs_1": {
                "kind": "PIPELINE",
                "type": "d3m.container.pandas.DataFrame"
            },
            "inputs_2": {
                "kind": "PIPELINE",
                "type": "d3m.container.pandas.DataFrame"
            },
            "iterations": {
                "default": null,
                "kind": "RUNTIME",
                "type": "typing.Union[NoneType, int]"
            },
            "params": {
                "kind": "RUNTIME",
                "type": "jhu_primitives.sgm_nomination.sgm_nomination.Params"
            },
            "produce_methods": {
                "kind": "RUNTIME",
                "type": "typing.Sequence[str]"
            },
            "random_seed": {
                "default": 0,
                "kind": "RUNTIME",
                "type": "int"
            },
            "reference": {
                "kind": "PIPELINE",
                "type": "d3m.container.pandas.DataFrame"
            },
            "timeout": {
                "default": null,
                "kind": "RUNTIME",
                "type": "typing.Union[NoneType, float]"
            }
        },
        "class_attributes": {
            "logger": "logging.Logger",
            "metadata": "d3m.metadata.base.PrimitiveMetadata"
        },
        "class_methods": {},
        "class_type_arguments": {
            "Hyperparams": "jhu_primitives.sgm_nomination.sgm_nomination.Hyperparams",
            "Inputs": "d3m.container.pandas.DataFrame",
            "Outputs": "d3m.container.pandas.DataFrame",
            "Params": "jhu_primitives.sgm_nomination.sgm_nomination.Params"
        },
        "hyperparams": {},
        "instance_attributes": {
            "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
            "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
            "random_seed": "int",
            "temporary_directory": "typing.Union[NoneType, str]",
            "volumes": "typing.Dict[str, str]"
        },
        "instance_methods": {
            "__init__": {
                "arguments": [
                    "hyperparams",
                    "random_seed",
                    "docker_containers"
                ],
                "kind": "OTHER",
                "returns": "NoneType"
            },
            "fit": {
                "arguments": [
                    "timeout",
                    "iterations"
                ],
                "description": "Fits primitive using inputs and outputs (if any) using currently set training data.\n\nThe returned value should be a ``CallResult`` object with ``value`` set to ``None``.\n\nIf ``fit`` has already been called in the past on different training data,\nthis method fits it **again from scratch** using currently set training data.\n\nOn the other hand, caller can call ``fit`` multiple times on the same training data\nto continue fitting.\n\nIf ``fit`` fully fits using provided training data, there is no point in making further\ncalls to this method with same training data, and in fact further calls can be noops,\nor a primitive can decide to fully refit from scratch.\n\nIn the case fitting can continue with same training data (even if it is maybe not reasonable,\nbecause the internal metric primitive is using looks like fitting will be degrading), if ``fit``\nis called again (without setting training data), the primitive has to continue fitting.\n\nCaller can provide ``timeout`` information to guide the length of the fitting process.\nIdeally, a primitive should adapt its fitting process to try to do the best fitting possible\ninside the time allocated. If this is not possible and the primitive reaches the timeout\nbefore fitting, it should raise a ``TimeoutError`` exception to signal that fitting was\nunsuccessful in the given time. The state of the primitive after the exception should be\nas the method call has never happened and primitive should continue to operate normally.\nThe purpose of ``timeout`` is to give opportunity to a primitive to cleanly manage\nits state instead of interrupting execution from outside. Maintaining stable internal state\nshould have precedence over respecting the ``timeout`` (caller can terminate the misbehaving\nprimitive from outside anyway). If a longer ``timeout`` would produce different fitting,\nthen ``CallResult``'s ``has_finished`` should be set to ``False``.\n\nSome primitives have internal fitting iterations (for example, epochs). For those, caller\ncan provide how many of primitive's internal iterations should a primitive do before returning.\nPrimitives should make iterations as small as reasonable. If ``iterations`` is ``None``,\nthen there is no limit on how many iterations the primitive should do and primitive should\nchoose the best amount of iterations on its own (potentially controlled through\nhyper-parameters). If ``iterations`` is a number, a primitive has to do those number of\niterations (even if not reasonable), if possible. ``timeout`` should still be respected\nand potentially less iterations can be done because of that. Primitives with internal\niterations should make ``CallResult`` contain correct values.\n\nFor primitives which do not have internal iterations, any value of ``iterations``\nmeans that they should fit fully, respecting only ``timeout``.\n\nParameters\n----------\ntimeout:\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA ``CallResult`` with ``None`` value.",
                "kind": "OTHER",
                "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]"
            },
            "fit_multi_produce": {
                "arguments": [
                    "produce_methods",
                    "inputs_1",
                    "inputs_2",
                    "reference",
                    "timeout",
                    "iterations"
                ],
                "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to ``set_training_data`` and all produce methods.\ntimeout:\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``.",
                "kind": "OTHER",
                "returns": "d3m.primitive_interfaces.base.MultiCallResult"
            },
            "get_params": {
                "arguments": [],
                "description": "Returns parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nParameters should include all data which is necessary to create a new instance of this primitive\nbehaving exactly the same as this instance, when the new instance is created by passing the same\nparameters to the class constructor and calling ``set_params``.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nReturns\n-------\nAn instance of parameters.",
                "kind": "OTHER",
                "returns": "jhu_primitives.sgm_nomination.sgm_nomination.Params"
            },
            "multi_produce": {
                "arguments": [
                    "produce_methods",
                    "inputs_1",
                    "inputs_2",
                    "reference",
                    "timeout",
                    "iterations"
                ],
                "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults. Despite accepting all arguments they can be passed as ``None`` by the caller\nwhen they are not needed by any of the produce methods in ``produce_methods``.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to all produce methods.\ntimeout:\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``.",
                "kind": "OTHER",
                "returns": "d3m.primitive_interfaces.base.MultiCallResult"
            },
            "produce": {
                "arguments": [
                    "inputs_1",
                    "inputs_2",
                    "reference",
                    "timeout",
                    "iterations"
                ],
                "description": "Produce primitive's best choice of the output for each of the inputs.\n\nThe output value should be wrapped inside ``CallResult`` object before returning.\n\nIn many cases producing an output is a quick operation in comparison with ``fit``, but not\nall cases are like that. For example, a primitive can start a potentially long optimization\nprocess to compute outputs. ``timeout`` and ``iterations`` can serve as a way for a caller\nto guide the length of this process.\n\nIdeally, a primitive should adapt its call to try to produce the best outputs possible\ninside the time allocated. If this is not possible and the primitive reaches the timeout\nbefore producing outputs, it should raise a ``TimeoutError`` exception to signal that the\ncall was unsuccessful in the given time. The state of the primitive after the exception\nshould be as the method call has never happened and primitive should continue to operate\nnormally. The purpose of ``timeout`` is to give opportunity to a primitive to cleanly\nmanage its state instead of interrupting execution from outside. Maintaining stable internal\nstate should have precedence over respecting the ``timeout`` (caller can terminate the\nmisbehaving primitive from outside anyway). If a longer ``timeout`` would produce\ndifferent outputs, then ``CallResult``'s ``has_finished`` should be set to ``False``.\n\nSome primitives have internal iterations (for example, optimization iterations).\nFor those, caller can provide how many of primitive's internal iterations\nshould a primitive do before returning outputs. Primitives should make iterations as\nsmall as reasonable. If ``iterations`` is ``None``, then there is no limit on\nhow many iterations the primitive should do and primitive should choose the best amount\nof iterations on its own (potentially controlled through hyper-parameters).\nIf ``iterations`` is a number, a primitive has to do those number of iterations,\nif possible. ``timeout`` should still be respected and potentially less iterations\ncan be done because of that. Primitives with internal iterations should make\n``CallResult`` contain correct values.\n\nFor primitives which do not have internal iterations, any value of ``iterations``\nmeans that they should run fully, respecting only ``timeout``.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\ninputs:\n    The inputs of shape [num_inputs, ...].\ntimeout:\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nThe outputs of shape [num_inputs, ...] wrapped inside ``CallResult``.",
                "inputs_across_samples": [],
                "kind": "PRODUCE",
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
                "singleton": false
            },
            "set_params": {
                "arguments": [
                    "params"
                ],
                "description": "Sets parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nParameters\n----------\nparams:\n    An instance of parameters.",
                "kind": "OTHER",
                "returns": "NoneType"
            },
            "set_training_data": {
                "arguments": [
                    "inputs_1",
                    "inputs_2",
                    "reference"
                ],
                "description": "Sets training data of this primitive.\n\nParameters\n----------\ninputs:\n    The inputs.",
                "kind": "OTHER",
                "returns": "NoneType"
            }
        },
        "interfaces": [
            "unsupervised_learning.UnsupervisedLearnerPrimitiveBase",
            "base.PrimitiveBase"
        ],
        "interfaces_version": "2021.11.25.dev0",
        "params": {
            "match": "d3m.container.numpy.ndarray"
        }
    },
    "primitive_family": "GRAPH_MATCHING",
    "python_path": "d3m.primitives.graph_matching.sgm_nomination.JHU",
    "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
    "source": {
        "contact": "mailto:asaadel1@jhu.edu",
        "name": "JHU",
        "uris": [
            "https://gitlab.com/datadrivendiscovery/contrib/jhu-primitives/-/blob/master/jhu_primitives/sgm_nomination/sgm_nomination.py",
            "https://gitlab.com/datadrivendiscovery/contrib/jhu-primitives"
        ]
    },
    "structural_type": "jhu_primitives.sgm_nomination.sgm_nomination.SgmNomination",
    "version": "0.1.0"
}

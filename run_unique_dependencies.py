#!/usr/bin/env python3

import argparse
import collections
import glob
import json
import sys
import traceback
from urllib import parse as url_parse


def process_directory(directory):
    known_dependencies = collections.defaultdict(dict)
    has_errored = False

    for primitive_annotation_path in glob.iglob('{directory}/*/*/*/primitive.json'.format(directory=directory)):
        try:
            with open(primitive_annotation_path, 'r', encoding='utf8') as primitive_annotation_file:
                primitive_annotation = json.load(primitive_annotation_file)

            for installation_entry in primitive_annotation['installation']:
                if installation_entry['type'] == 'PIP' and 'package' in installation_entry:
                    known_dependencies[installation_entry['package']][installation_entry['version']] = primitive_annotation_path
                elif installation_entry['type'] == 'PIP' and 'package_uri' in installation_entry:
                    # git+git scheme is not supported, and other URIs can be parsed with urlparse.
                    parsed_uri = url_parse.urlparse(installation_entry['package_uri'])

                    if not parsed_uri.scheme.startswith('git'):
                        # Not a git URI. We cannot do checking.
                        # TODO: Maybe improves if we have more data about reproducibility, e.g., hash of the target file.
                        #       See: https://gitlab.com/datadrivendiscovery/primitives/-/issues/395
                        pass
                    else:
                        fragment = url_parse.parse_qs(parsed_uri.fragment)
                        # This raises an exception if "egg" fragment is missing.
                        if len(fragment['egg']) != 1:
                            raise Exception("Invalid egg fragment value: {fragment}".format(fragment=fragment))
                        # In validator we check that all egg values really match the package name.
                        package_name = fragment['egg'][0]
                        known_dependencies[package_name][installation_entry['package_uri']] = primitive_annotation_path
        except Exception:
            print("Error at primitive '{primitive_annotation_path}'.".format(primitive_annotation_path=primitive_annotation_path), flush=True)
            traceback.print_exc()
            sys.stdout.flush()
            has_errored = True

    for package_name, versions in known_dependencies.items():
        if len(versions) > 1:
            has_errored = True
            print("Error: Same package '{package_name}' installed with different versions: {versions}".format(
                package_name=package_name,
                versions=', '.join("'{version}' from '{primitive_annotation_path}'".format(version=version, primitive_annotation_path=primitive_annotation_path) for version, primitive_annotation_path in versions.items()),
            ), flush=True)

    return has_errored


def main():
    parser = argparse.ArgumentParser(description="Check that primitives have unique Python dependencies.")
    parser.add_argument('directories', metavar='DIR', nargs='*', help="directories to check primitives in", default=())
    arguments = parser.parse_args()

    has_errored = False
    for directory in arguments.directories:
        has_errored = process_directory(directory) or has_errored

    if has_errored:
        sys.exit(1)


if __name__ == '__main__':
    main()
